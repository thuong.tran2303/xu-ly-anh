import numpy as np
import math 
from io import StringIO
f = open('iris.data', 'r')
str = f.read()
data = StringIO(str)
data = np.genfromtxt(data, comments = ",Iris", delimiter=",")
a = data[:,0]
mean = np.sum(a)/a.shape[0]
print("mean = ", round(mean, 3))
b = np.sort(a)
c = b.shape[0]
if c%2==0:
	meadian = (b[int((c/2)-1)] + b[int(c/2)])/2
else:
	meadian = b[int((c/2) - 0.5)]
print("meadian = ",round(meadian, 3))
d = 0.0
for i in range(c):
	d += math.sqrt((b[i]-mean)*(b[i]-mean)/(c-1))
print("standard_deviation = ", round(d, 3))