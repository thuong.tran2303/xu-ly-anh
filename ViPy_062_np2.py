import numpy as np
import math 
from io import StringIO

f = open('iris.data', 'r')
str = f.read()
data = StringIO(str)
data = np.genfromtxt(data, comments = ",Iris", delimiter=",")
data[1,:] = np.nan
data[100,:] = np.nan
for i in range(data.shape[0]):
	for j in range(data.shape[1]):
		if data[i,j] == np.nan:
			data[i,j] = 0
print(data)
