import numpy as np
import math 
from io import StringIO

f = open('iris.data', 'r')
string = f.read()
data = StringIO(string)
data = np.genfromtxt(data, comments = ",Iris", delimiter=",")
a = data[:, 2]
print(repr(a))
b = []
c1 = np.array(['small'])
c2 = np.array(['medium'])
c3 = np.array(['large'])
for i in range(a.shape[0]):
	if a[a.shape[0]-i-1] < 3:
		b = np.concatenate((c1, b))
	elif a[a.shape[0]-i-1] >= 5:
		b = np.concatenate((c3, b))
	else:
		b = np.concatenate((c2, b))
print(repr(b))