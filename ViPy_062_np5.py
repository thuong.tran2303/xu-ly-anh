import numpy as np
import math 
from io import StringIO
from collections import Counter

f = open('iris.data', 'r')
string = f.read()
data = StringIO(string)
data = np.genfromtxt(data, comments = ",Iris", delimiter=",")
a = data[:, 2]
b = 0
max_lap = 0
c = 0
for i in range(a.shape[0]):
	for j in range(a.shape[0]):
		if a[i] == a[j]:
			b += 1
	if b > max_lap:
		max_lap = b
		c = a[i]
	b = 0
print(max_lap,',',"{:.2f}".format(round(c, 2)))