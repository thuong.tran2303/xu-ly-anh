import numpy as np
b = np.random.seed(101)
a = []
arr = np.random.randint(1,4, size=6)
for i in range(len(arr)):
	if arr[i] == 1:
		a = np.concatenate((a,[1., 0., 0.]))
	if arr[i] == 2:
		a = np.concatenate((a,[0., 1., 0.]))
	if arr[i] == 3:
		a = np.concatenate((a,[0., 0., 1.]))
print(repr(a.reshape(arr.shape[0],3)))